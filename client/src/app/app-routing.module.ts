import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from "./form/form.component"
import { ConnectedComponent } from "./connected/connected.component"

const routes: Routes = [
    {
        path: "",
        component: FormComponent,
    },
    {
        path: "user",
        component: ConnectedComponent,
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
