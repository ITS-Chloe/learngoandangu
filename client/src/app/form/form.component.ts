import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms'
import { HttpClient } from "@angular/common/http"
import {Router} from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  loginForm;

  constructor(
      private http: HttpClient,
      private router: Router,
      private formBuilder: FormBuilder
  ) {
    this.loginForm = this.formBuilder.group({
        username: '',
        password: ''
    });
  }

  ngOnInit(): void {
  }

  onSubmit(loginInfo) {
      console.log(JSON.stringify(loginInfo))
      this.http.post("http://0.0.0.0:3000/form", JSON.stringify(loginInfo),{observe: 'response'})
            .subscribe(response => {
                if (response.status == 200)
                    this.router.navigateByUrl('/user')
            })
  }

}
