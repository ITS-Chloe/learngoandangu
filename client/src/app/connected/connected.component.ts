import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http"

@Component({
  selector: 'app-connected',
  templateUrl: './connected.component.html',
  styleUrls: ['./connected.component.css']
})
export class ConnectedComponent implements OnInit {

    username : string;

  constructor(
      private http: HttpClient,

  ) { }

  private getUser() {
      this.http.get("http://0.0.0.0:3000/user", {observe: 'body'})
          .subscribe(body => {
              var data = body as any
              this.username = data.Username
          })
  }

  ngOnInit(): void {
      this.getUser()
  }

}
