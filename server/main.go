package main
import (
    "time"
    "fmt"
    "os"
    "log"
    "flag"
    "context"
    "net/http"
    "os/signal"
    "encoding/json"

    "github.com/gorilla/handlers"
    "github.com/gorilla/mux"
)

type LogIn struct {
    Username string
    Password string
}
var p LogIn
var connected = false

func user(w http.ResponseWriter, r *http.Request) {
    if (connected == false) {
        http.Error(w, "No user logged", http.StatusNotFound)
	    return
    }
    w.Header().Set("Content-type", "application/json;charset=UTF-8")
    w.WriteHeader(http.StatusOK)
    json.NewEncoder(w).Encode(p)
}

func form(w http.ResponseWriter, r *http.Request) {

    r.Body = http.MaxBytesReader(w, r.Body, 1048576)

    dec := json.NewDecoder(r.Body)
    dec.DisallowUnknownFields()

    err := dec.Decode(&p)
    if err != nil {
        fmt.Println(err)
        http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
        return
    }
    connected = true
    w.WriteHeader(http.StatusOK)
}

func main() {
    var wait time.Duration
    flag.DurationVar(&wait, "graceful-timeout", time.Second * 15, "the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m")
    flag.Parse()

    router := mux.NewRouter()
    router.HandleFunc("/form", form).Methods("POST")
    router.HandleFunc("/user", user).Methods("GET")

    cors := handlers.CORS(
            handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD"}),
            handlers.AllowedOrigins([]string{"*"}))

    router.Use(cors)

    srv := &http.Server{
        Addr: "0.0.0.0:3000",
        WriteTimeout: time.Second * 15,
        ReadTimeout: time.Second * 15,
        IdleTimeout: time.Second * 60,
        Handler: router,
    }

    go func() {
        fmt.Println("Server run at http://localhost:3000")
        if err := srv.ListenAndServe(); err != nil {
     	    log.Println(err)
        }
    }()
    c := make(chan os.Signal, 1)
    signal.Notify(c, os.Interrupt)
    <- c

    ctx, cancel := context.WithTimeout(context.Background(), wait)
    defer cancel()
    srv.Shutdown(ctx)
    log.Println("Shutting down")
    os.Exit(0)
}
